package com.totoro.project.dao;

import com.totoro.project.model.Demo;

public interface DemoDao {
    int insert(Demo demo);
}
