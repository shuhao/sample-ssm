package com.totoro.project.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.totoro.project.service.DemoService;

@Controller
public class DemoController {
    @Resource
    DemoService service ;
    
    @RequestMapping("/demo")
    @ResponseBody
    public String demo(){
        service.test();
        service.addDemo();
        return "demo" ;
    }
    
    @RequestMapping("demo2")
    public String demo2(){
        return "hello" ;
    }
}
