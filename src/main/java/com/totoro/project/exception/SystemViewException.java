package com.totoro.project.exception;

import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver ;

/**
 * 页面统一异常处理类，区别页面请求和json形式访问
 * @author 80002165
 * @date 2017年3月22日 下午6:14:42
 */
public class SystemViewException extends SimpleMappingExceptionResolver{
    
}

