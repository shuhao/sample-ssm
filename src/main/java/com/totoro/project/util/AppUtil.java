package com.totoro.project.util;

import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class AppUtil implements ApplicationContextAware{
    private static ApplicationContext applicationContext; 
    public void setApplicationContext(ApplicationContext arg0) throws BeansException {
    }
    
    /**
     * 通过Bean的id获取bean
     * @author niki
     * @date 2017年3月23日 下午9:14:21
     * @param id
     * @return
     */
    public static Object getBean(String id){
        return applicationContext.getBean(id) ;
    }
    
    /**
     * 获取spring上下文
     * @author niki
     * @date 2017年3月23日 下午9:14:53
     * @return
     */
    public static ApplicationContext getApplicationContext(){
        return applicationContext ;
    }
    
    /**
     * 根据bean的class来查找所有的对象(包括子类)
     * @param c
     * @return
     */
    public static Map getBeansByClass(Class c){
        return applicationContext.getBeansOfType(c);
    }

}
