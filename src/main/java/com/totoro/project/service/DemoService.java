package com.totoro.project.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.totoro.project.dao.DemoDao;
import com.totoro.project.model.Demo;

/**
 * 测试service 
 * @author niki
 * @date 2017年3月23日 下午8:05:33
 */
@Service
public class DemoService {
    @Resource
    DemoDao dao ;
    public void test(){
        System.out.println("testService");
    }
    
    public void addDemo(){
        Demo demo = new Demo() ;
        demo.setId(1); 
        demo.setName("方书豪");
        dao.insert(demo) ;
    }
}
